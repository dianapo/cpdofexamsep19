package com.agiletestingalliance;

public class MinMax {

    public int getMax(int numberOne, int numberTwo) {
        if (numberTwo > numberOne) {
            return numberTwo;
        } else {
            return numberOne;
        }
    }

}
